package com.models;

import com.connection.Conexion;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Nombre de la clase: Usuario
 * Fecha: 24/09/2018
 * @version 1.0
 * Copyright: ITCA-FEPADE
 * @author Juan Pablo Elias Hernández
 */
public class DaoUsuario extends Conexion
{
    public List all (String user) throws Exception
    {
        List ls = new ArrayList();
        ResultSet rs;
        try
        {
            this.conectar();
            String sql = "SELECT U.id, U.idTipoUsuario AS tu, " +
                            "U.nombre AS User, CAST(AES_DECRYPT(U.pass, 'Key') " +
                            "AS CHAR(50)) AS Pass, U.estado, TU.descripcion AS NombreTu " +
                            "FROM Usuarios U INNER JOIN TiposUsuario TU " +
                            "ON U.idTipoUsuario = TU.id WHERE U.nombre != ? ORDER BY U.id DESC;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, user);
            rs = pst.executeQuery();
            while(rs.next())
            {
                Usuario u = new Usuario();
                u.setId(rs.getInt(1));
                u.setTu(rs.getInt(2));
                u.setNombre(rs.getString(3));
                u.setPass(rs.getString(4));
                u.setEstado(rs.getInt(5));
                u.setNombreTu(rs.getString(6));
                ls.add(u);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            desconectar();
        }
        return ls;
    }
    
    public void add(Usuario u) throws Exception 
    {
        try 
        {
            this.conectar();
            String sql = "INSERT INTO Usuarios VALUES(NULL, ?, ?, AES_ENCRYPT(?, 'Key'), ?);";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, u.getTu());
            pst.setString(2, u.getNombre());
            pst.setString(3, u.getPass());
            pst.setInt(4, u.getEstado());
            pst.executeUpdate();
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void edit(Usuario u) throws Exception
    {
        try 
        {
            this.conectar();
            String sql = "UPDATE Usuarios SET idTipoUsuario = ?, nombre = ?, "
                    + "pass = AES_ENCRYPT(?, 'Key'), estado = ? WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, u.getTu());
            pst.setString(2, u.getNombre());
            pst.setString(3, u.getPass());
            pst.setInt(4, u.getEstado());
            pst.setInt(5, u.getId());
            pst.executeUpdate();
        } 
        catch (Exception e)
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public void delete(Usuario u) throws Exception
    {
        try
        {
            this.conectar();
            String sql = "DELETE FROM Usuarios WHERE id = ?;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setInt(1, u.getId());
            pst.executeUpdate();
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
    }
    
    public Integer validateUser(Usuario u) throws Exception
    {
        ResultSet rs;
        Integer tu = null;
        try
        {
            this.conectar();
            String sql = "SELECT U.idTipoUsuario FROM Usuarios U WHERE "
                    + "U.pass = AES_ENCRYPT(?, 'Key') AND U.nombre = ? AND estado = 1;";
            PreparedStatement pst = this.getConn().prepareStatement(sql);
            pst.setString(1, u.getPass());
            pst.setString(2, u.getNombre());
            rs = pst.executeQuery();
            if (rs.next())
            {
                tu = rs.getInt(1);
            }
        }
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return tu;
    }
}
