package com.models;

/**
 * Nombre de la clase: Vendedor
 * Fecha: 24/09/2018
 * Version: 1.0
 * Copyrigth: Alvaro Perez 
 * Autor : Perez
 */
public class Vendedor
{
    private int id;
    private String dui;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String telOficina;
    private String telMovil;

    public Vendedor()
    {
        
    }

    public Vendedor(int id, String dui, String nombre, String apellidos, String direccion, String telefonoOficina, String telefonoMovil) {
        this.id = id;
        this.dui = dui;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telOficina = telefonoOficina;
        this.telMovil = telefonoMovil;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelOficina() {
        return telOficina;
    }

    public void setTelOficina(String telOficina) {
        this.telOficina = telOficina;
    }

    public String getTelMovil() {
        return telMovil;
    }

    public void setTelMovil(String telMovil) {
        this.telMovil = telMovil;
    }

    
    
    
}
