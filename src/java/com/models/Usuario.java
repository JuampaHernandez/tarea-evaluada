package com.models;

/**
 * Nombre de la clase: Usuario
 * Fecha: 24/09/2018
 * @version 1.0
 * Copyrigth: ITCA-FEPADE
 * @author Juan Pablo Elias Hernandez
 */
public class Usuario
{
    private Integer id;
    private Integer tu;
    private String nombre;
    private String pass;
    private Integer estado;
    private String nombreTu;

    public Usuario() {
    }

    public Usuario(Integer id, Integer tu, String nombre, String pass, Integer estado) {
        this.id = id;
        this.tu = tu;
        this.nombre = nombre;
        this.pass = pass;
        this.estado = estado;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTu() {
        return tu;
    }

    public void setTu(Integer tu) {
        this.tu = tu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNombreTu() {
        return nombreTu;
    }

    public void setNombreTu(String nombreTu) {
        this.nombreTu = nombreTu;
    }
    
}
