package com.controllers;

import com.models.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Nombre del servlet: ControllerProducto
 Fecha: 24/09/2018
 Version: 1.0
 Copyrigth: Alvaro Perez 
 Autor : Perez
 */
public class ControllerProducto extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Producto p = new Producto();
        DaoProducto daoP = new DaoProducto();
        String msj ="";
        RequestDispatcher rd = null;
        try
        {
            p.setId(Integer.parseInt(request.getParameter("txtId")));
            p.setNombre(request.getParameter("txtNombre"));
            p.setPrecioVenta(Double.parseDouble(request.getParameter("txtPrecio")));
            p.setStockMin(Integer.parseInt(request.getParameter("txtStockMinimo")));
            p.setStockAct(Integer.parseInt(request.getParameter("txtStockActual")));
            p.setCodigoBarra(request.getParameter("txtCodigoBarra"));

            if(request.getParameter("btnAdd") != null)
            {
                daoP.agregar(p);
                msj = "Datos guardados correctamente en la base de datos.";
            }
            else if(request.getParameter("action") != null
                    && "editar".equals(request.getParameter("action")))
            {
                daoP.modificar(p);
                msj = "Datos editados correctamente en la base de datos.";
            }
            else if(request.getParameter("action") != null
                    && "eliminar".equals(request.getParameter("action")))
            {
                daoP.eliminar(p);
                msj = "Datos eliminados correctamente de la base de datos.";
            }
            request.getSession().setAttribute("info", msj);
        }
        catch(Exception e)
        {
            request.getSession().setAttribute("Error", "El código de barras debe ser único");
        }
        response.sendRedirect("producto.jsp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
