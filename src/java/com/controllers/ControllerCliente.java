package com.controllers;

import com.models.Cliente;
import com.models.DaoCliente;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kevin Lovos
 */
public class ControllerCliente extends HttpServlet 
{

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DaoCliente daoC = new DaoCliente();
        Cliente cli = new Cliente();
        String info = null;
        try
        {
            cli.setId(Integer.parseInt(request.getParameter("cliId")));
            cli.setNombre(request.getParameter("nom"));
            cli.setApellidos(request.getParameter("ape"));
            cli.setDireccion(request.getParameter("dir"));
            if(request.getParameter("btnNew") != null)
            {
                daoC.addCliente(cli);
                info = "Datos guardados correctamente en la base de datos.";
            }
            else if (request.getParameter("action") != null
                    && "editar".equals(request.getParameter("action")))
            {
                daoC.editCliente(cli);
                info = "Datos editados correctamente en la base de datos.";
            }
            else if (request.getParameter("action") != null
                    && "eliminar".equals(request.getParameter("action")))
            {
                daoC.deleteCliente(cli);
                info = "Datos eliminados correctamente de la base de datos.";
            }
            request.getSession().setAttribute("info", info);
        } 
        catch (Exception e)
        {
            request.getSession().setAttribute("Error", e.toString());
        }
        response.sendRedirect("cliente.jsp"); 
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
