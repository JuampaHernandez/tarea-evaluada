package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import com.models.*;

public final class usuario_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
    Usuario cli = new Usuario();
    DaoUsuario daoC = new DaoUsuario();

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n");
      out.write("        <link rel=\"icon\" type=\"image/png\" href=\"resources/img/Icon-shop.png\" sizes=\"64x64\">\n");
      out.write("        <link href=\"resources\\css\\bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <link href=\"resources/css/sweetalert2.min.css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("        <link href=\"resources/css/datatables.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <link href=\"resources/css/dataTables.bootstrap4.css\"/>\n");
      out.write("        <link href=\"resources/css/buttons.bootstrap4.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <title>Usuarios</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">\n");
      out.write("            <a class=\"navbar-brand\" href=\"#\">\n");
      out.write("                <i class=\"fas fa-store\"></i>\n");
      out.write("                Tienda\n");
      out.write("            </a>\n");
      out.write("            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n");
      out.write("                <span class=\"navbar-toggler-icon\"></span>\n");
      out.write("            </button>\n");
      out.write("\n");
      out.write("            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n");
      out.write("                <ul class=\"navbar-nav mr-auto\">\n");
      out.write("                    <li class=\"nav-item\">\n");
      out.write("                        <a class=\"nav-link\" href=\"index.jsp\">\n");
      out.write("                            <i class=\"fas fa-home\"></i>\n");
      out.write("                            Inicio <span class=\"sr-only\">(current)</span>\n");
      out.write("                        </a>\n");
      out.write("                    </li>\n");
      out.write("                    <li class=\"nav-item\">\n");
      out.write("                        <a class=\"nav-link\" href=\"producto.jsp\">\n");
      out.write("                            <i class=\"fas fa-shopping-basket\"></i>\n");
      out.write("                            Productos\n");
      out.write("                        </a>\n");
      out.write("                    </li>\n");
      out.write("                    <li class=\"nav-item\">\n");
      out.write("                        <a class=\"nav-link\" href=\"vendedor.jsp\">\n");
      out.write("                            <i class=\"fas fa-id-card-alt\"></i>\n");
      out.write("                            Vendedores\n");
      out.write("                        </a>\n");
      out.write("                    </li>\n");
      out.write("                    <li class=\"nav-item active\">\n");
      out.write("                        <a class=\"nav-link\" href=\"cliente.jsp\">\n");
      out.write("                            <i class=\"fas fa-users\"></i>\n");
      out.write("                            Clientes\n");
      out.write("                        </a>\n");
      out.write("                    </li>\n");
      out.write("                    <li class=\"nav-item\">\n");
      out.write("                        <a class=\"nav-link\" href=\"venta.jsp\">\n");
      out.write("                            <i class=\"fas fa-receipt\"></i>\n");
      out.write("                            Venta\n");
      out.write("                        </a>\n");
      out.write("                    </li>\n");
      out.write("                </ul>\n");
      out.write("                <form action=\"controllerUsuario\" id=\"closeFrm\" method=\"post\" class=\"form-inline my-2 my-lg-0\">\n");
      out.write("                    <input type=\"text\" name=\"close\" value=\"close\" style=\"display: none;\">\n");
      out.write("                    <button class=\"btn btn-outline-light btn-sm my-2 my-sm-0\" \n");
      out.write("                            name=\"close\" value=\"close\" type=\"button\" id=\"closeSession\">\n");
      out.write("                        <i class=\"fas fa-sign-out-alt\"></i>\n");
      out.write("                        Cerrar sesion\n");
      out.write("                    </button>\n");
      out.write("                </form>\n");
      out.write("            </div>\n");
      out.write("        </nav>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <br>\n");
      out.write("            <br>\n");
      out.write("            <h1 align=\"center\" class=\"text-dark\">\n");
      out.write("                <i class=\"fas fa-users\"></i>\n");
      out.write("                Clientes\n");
      out.write("            </h1>\n");
      out.write("            <br>\n");
      out.write("            ");

                if (idTu == 1)
                {
            
      out.write("\n");
      out.write("            <form action=\"controllerCliente\" id=\"frm\" method=\"post\" class=\"needs-validation\" novalidate>\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group col-sm-12\" style=\"display: none;\">\n");
      out.write("                        <label for=\"cliId\">ID:</label>\n");
      out.write("                        <input value=\"0\" type=\"number\" id=\"cliId\" class=\"form-control\" name=\"cliId\" required readonly>\n");
      out.write("                        <input type=\"text\" name=\"action\" id=\"action\"/>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-sm-12 col-md-4\">\n");
      out.write("                        <label for=\"nom\">Nombre:</label>\n");
      out.write("                        <input type=\"text\" placeholder=\"Nombre\" id=\"nom\" class=\"form-control\" name=\"nom\" required>\n");
      out.write("                        <div class=\"invalid-feedback\">\n");
      out.write("                            Por favor, introduzca el nombre del cliente.\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-sm-12 col-md-4\">\n");
      out.write("                        <label for=\"ape\">Apellidos:</label>\n");
      out.write("                        <input type=\"text\" placeholder=\"Apellidos\" id=\"ape\" class=\"form-control\" name=\"ape\" required>\n");
      out.write("                        <div class=\"invalid-feedback\">\n");
      out.write("                            Por favor, introduzca los apellidos del cliente.\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-sm-12 col-md-4\">\n");
      out.write("                        <label for=\"dir\">Direccion:</label>\n");
      out.write("                        <input type=\"text\" placeholder=\"Direccion\" id=\"dir\" class=\"form-control\" name=\"dir\" required>\n");
      out.write("                        <div class=\"invalid-feedback\">\n");
      out.write("                            Por favor, introduzca la dirección del cliente.\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"row justify-content-center\">\n");
      out.write("                    <button type=\"submit\" class=\"btn btn-outline-success\" name='btnNew' value=\"x\">\n");
      out.write("                        <i class=\"fas fa-plus\"></i>\n");
      out.write("                        Nuevo\n");
      out.write("                    </button>\n");
      out.write("                    &nbsp;\n");
      out.write("                    <button type=\"button\" name=\"btnEdit\" \n");
      out.write("                            class=\"btn btn-outline-info btnAction\" value=\"editar\">\n");
      out.write("                        <i class=\"fas fa-pencil-alt\"></i>\n");
      out.write("                        Editar\n");
      out.write("                    </button>\n");
      out.write("                    &nbsp;\n");
      out.write("                    <button type=\"button\" name=\"btnDelete\" \n");
      out.write("                            class=\"btn btn-outline-danger btnAction\" value=\"eliminar\">\n");
      out.write("                        <i class=\"fas fa-trash\"></i>\n");
      out.write("                        Eliminar\n");
      out.write("                    </button>\n");
      out.write("                    &nbsp;\n");
      out.write("                    <button type=\"reset\" class=\"btn btn-outline-secondary\">\n");
      out.write("                        <i class=\"fas fa-undo\"></i>\n");
      out.write("                        Cancelar\n");
      out.write("                    </button>\n");
      out.write("                </div>\n");
      out.write("            </form>\n");
      out.write("            ");

                }
            
      out.write("\n");
      out.write("            <br>\n");
      out.write("            <!-- Example DataTables Card -->\n");
      out.write("            <div class=\"card mb-3\">\n");
      out.write("                <div class=\"card-header\">\n");
      out.write("                    <i class=\"fa fa-table\"></i> Listado de Clientes\n");
      out.write("                </div>\n");
      out.write("                <div class=\"card-body\">\n");
      out.write("                    <div class=\"table-responsive\">\n");
      out.write("                        <table class=\"table table-hover\" id=\"cliTable\" width=\"100%\" cellspacing=\"0\">\n");
      out.write("                            <thead>\n");
      out.write("                                <tr>\n");
      out.write("                                    <th>Nombre</th>\n");
      out.write("                                    <th>Apellidos</th>\n");
      out.write("                                    <th>Estado</th>\n");
      out.write("                                    <th>Tipo Usuario</th>\n");
      out.write("                                    <th>Acciones</th>\n");
      out.write("                                </tr>\n");
      out.write("                            </thead>\n");
      out.write("                            <tfoot>\n");
      out.write("                                <tr>\n");
      out.write("                                    <th>Nombre</th>\n");
      out.write("                                    <th>Contraseña</th>\n");
      out.write("                                    <th>Estado</th>\n");
      out.write("                                    <th>Tipo Usuario</th>\n");
      out.write("                                    <th>Acciones</th>\n");
      out.write("                                </tr>\n");
      out.write("                            </tfoot>\n");
      out.write("                            <tbody>\n");
      out.write("                                ");

                                    List <Usuario> c = daoC.all();
                                    for (Usuario cl : c)
                                    {
                                
      out.write("\n");
      out.write("                                <tr>\n");
      out.write("                                    <td>");
      out.print( cl.getNombre() );
      out.write("</td>\n");
      out.write("                                    <td>");
      out.print( cl.getPass());
      out.write("</td>\n");
      out.write("                                    <td>");
      out.print( cl.getEstado());
      out.write("</td>\n");
      out.write("                                    <td>");
      out.print( cl.getNombreTu());
      out.write("</td>\n");
      out.write("                                    <td class=\"justify-content-center\">\n");
      out.write("                                        <a class=\"btn btn-outline-primary btn-sm col-12 select\" href=\"javascript:load(");
      out.print( cl.getId() );
      out.write(", '");
      out.print( cl.getNombre() );
      out.write("', '");
      out.print( cl.getPass() );
      out.write("', ");
      out.print( cl.getEstado());
      out.write(',');
      out.write(' ');
      out.print( cl.getTu());
      out.write(")\"> \n");
      out.write("                                            Seleccionar\n");
      out.write("                                        </a>\n");
      out.write("                                    </td>\n");
      out.write("                                </tr>\n");
      out.write("                                ");

                                    }
                                
      out.write("\n");
      out.write("                            </tbody>\n");
      out.write("                        </table>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"card-footer small text-muted\">\n");
      out.write("                    Última actualización: \n");
      out.write("                    ");

                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                        String formatDateTime = LocalDateTime.now().format(formatter);
                        out.print(formatDateTime);
                    
      out.write("\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <script src=\"resources/js/jquery-3.3.1.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources\\js\\popper.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources\\js\\bootstrap.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources\\js\\fontawesome-all.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources\\js\\sweetalert2.all.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources/js/datatables.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources/js/dataTables.bootstrap4.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources/js/buttons.bootstrap4.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources/js/cliente.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"resources/js/globalConfig.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script>\n");
      out.write("        ");

            if (request.getSession().getAttribute("info") != null)
            {
                String info = (String) request.getSession().getAttribute("info");
        
      out.write("\n");
      out.write("            swal('Realizado!', '");
      out.print( info );
      out.write("', 'success');\n");
      out.write("            console.log('Mensaje');\n");
      out.write("        ");
        
            }
            request.getSession().setAttribute("info", null);
            // Error handler
            if (request.getSession().getAttribute("Error") != null)
            {
                String error = (String) request.getSession().getAttribute("Error");
        
      out.write("\n");
      out.write("            swal({\n");
      out.write("                type: 'error',\n");
      out.write("                title: 'Error al realizar la operación',\n");
      out.write("                text: \"Ocurrió el siguiente error: \\n\" + \"");
      out.print( error );
      out.write("\"\n");
      out.write("            })\n");
      out.write("        ");
        
            }
            request.getSession().setAttribute("Error", null);
        
      out.write("\n");
      out.write("        </script>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
