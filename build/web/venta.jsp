<%-- 
    Document   : index
    Created on : 23/09/2018, 03:25:57 PM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.models.*"%>
<%@page import="java.time.LocalDateTime"%>
<%@page session="true" %>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
    DaoFactura daoF = new DaoFactura();
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/png" href="resources/img/Icon-shop.png" sizes="64x64">
        <link rel="stylesheet" href="resources\css\bootstrap.min.css">
        <link href="resources/css/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/datatables.css" rel="stylesheet" type="text/css"/>
        <link href="resources/css/dataTables.bootstrap4.css"/>
        <link href="resources/css/buttons.bootstrap4.css" rel="stylesheet" type="text/css"/>
        <title>Venta</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">
                <i class="fas fa-store"></i>
                Tienda
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.jsp">
                            <i class="fas fa-home"></i>
                            Inicio <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producto.jsp">
                            <i class="fas fa-shopping-basket"></i>
                            Productos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="vendedor.jsp">
                            <i class="fas fa-id-card-alt"></i>
                            Vendedores
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cliente.jsp">
                            <i class="fas fa-users"></i>
                            Clientes
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="venta.jsp">
                            <i class="fas fa-receipt"></i>
                            Venta
                        </a>
                    </li>
                    <%
                        if (idTu == 1)
                        {
                    %>
                    <li class="nav-item">
                        <a class="nav-link" href="usuario.jsp">
                            <i class="fas fa-user"></i>
                            Usuarios
                        </a>
                    </li>
                    <%
                        }
                    %>
                </ul>
                <form action="controllerUsuario" id="closeFrm" method="post" class="form-inline my-2 my-lg-0">
                    <input type="text" name="close" value="close" style="display: none;">
                    <button class="btn btn-outline-light btn-sm my-2 my-sm-0" 
                            name="close" value="close" type="button" id="closeSession">
                        <i class="fas fa-sign-out-alt"></i>
                        Cerrar sesion
                    </button>
                </form>
            </div>
        </nav>
        <div class="container">
            <br>
            <br>
            <h1 align="center" class="text-dark">
                <i class="fas fa-receipt"></i>
                Ventas / Facturas
            </h1>
            <br>
            <%
                if (idTu == 1 || idTu == 2)
                {
            %>
            <form action="controllerFactura" method='post' class="needs-validation" novalidate>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="fId">ID:</label>
                        <input value="0" type="number" id="fId" class="form-control" name="fId" required readonly>
                        <div class="invalid-feedback">
                            Por favor, introduzca su ID.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="num">Número de Factura:</label>
                        <input type="text" placeholder="Número de factura" id="num" class="form-control" name="num" required>
                        <div class="invalid-feedback">
                            Por favor, genere un número de factura.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="cId">Cliente:</label>
                        <select id="cId" class="form-control" name="cId" required>
                            <option value="">-- Seleccionar --</option>
                        <%
                            List <Cliente> lsC = daoF.allClientes();
                            for (Cliente c : lsC)
                            {
                        %>
                            <option value="<%= c.getId() %>"><%= c.getNombre() %></option>
                        <%
                            } 
                        %>
                        </select>
                        <div class="invalid-feedback">
                            Por favor, seleccione un cliente.
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="vId">Venderdor:</label>
                        <select id="vId" class="form-control" name="vId" required>
                            <option value="">-- Seleccionar --</option>
                        <%
                            List <Vendedor> lsV = daoF.allVendedores();
                            for (Vendedor v : lsV)
                            {
                        %>
                            <option value="<%= v.getId() %>"><%= v.getNombre() %></option>
                        <%
                            } 
                        %>
                        </select>
                        <div class="invalid-feedback">
                            Por favor, seeccione un vendedor.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="total">Total:</label>
                        <input type="number" id="total" step="any" value="0.0" 
                            class="form-control" name="total" readonly>
                        <div class="invalid-feedback">
                            Por favor, agregue productos.
                        </div>
                    </div>
                    <div class="form-group col-sm-12 col-md-4">
                        <label for="fecha">Fecha:</label>
                        <input type="date" id="fecha" class="form-control" name="fecha" required readonly>
                        <div class="invalid-feedback">
                            Por favor, introduzca una fecha.
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <button type="submit" name='btnAdd' class="btn btn-outline-success btnAdd" value="x">
                        <i class="fas fa-save"></i>
                        Guardar
                    </button>
                    &nbsp;
                    <button type="button" name='btnCode' class="btn btn-outline-info btnCode" value="x">
                        <i class="fas fa-barcode"></i>
                        Generar codigo factura
                    </button>
                    &nbsp;
                    <button type='button' class="btn btn-outline-secondary btnCancel">
                        <i class="fas fa-undo"></i>
                        Cancelar
                    </button>
                </div>
            </form>
            <br>
            <%
                }
            %>
            <!-- Example DataTables Card -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i> Listado de ventas / facturas</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Codigo Factura</th>
                                    <th>Cliente</th>
                                    <th>Vendedor</th>
                                    <th>Total</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Codigo Factura</th>
                                    <th>Cliente</th>
                                    <th>Vendedor</th>
                                    <th>Total</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <%
                                    List <Factura> lsF = daoF.all();
                                    for (Factura f : lsF)
                                    {
                                %>
                                <tr>
                                    <td><%= f.getNumFactura() %></td>
                                    <td><%= f.getCliente().getNombre() %></td>
                                    <td><%= f.getVendedor().getNombre() %></td>
                                    <td><%= f.getTotal() %></td>
                                    <td><%= f.getFecha() %></td>
                                    <td>
                                        <button type="button" data-num="<%= f.getNumFactura() %>" 
                                                class="btn btn-outline-primary btn-sm col-12 seeDetails">
                                            Ver detalles
                                        </button>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">
                    Última actualización: <%= LocalDateTime.now() %>
                </div>
            </div>
        </div>
        <script src="resources/js/jquery-3.3.1.js" type="text/javascript"></script>
        <script src="resources\js\popper.min.js" type="text/javascript"></script>
        <script src="resources\js\bootstrap.min.js" type="text/javascript"></script>
        <script src="resources\js\fontawesome-all.js" type="text/javascript"></script>
        <script src="resources\js\sweetalert2.all.min.js" type="text/javascript"></script>
        <script src="resources/js/datatables.js" type="text/javascript"></script>
        <script src="resources/js/dataTables.bootstrap4.js" type="text/javascript"></script>
        <script src="resources/js/buttons.bootstrap4.js" type="text/javascript"></script>
        <script src="resources/js/momentjs.js" type="text/javascript"></script>
        <script src="resources/js/venta.js" type="text/javascript"></script>
        <script src="resources/js/globalConfig.js" type="text/javascript"></script>

    </body>
</html>
