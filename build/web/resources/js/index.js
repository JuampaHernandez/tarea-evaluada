(function ()
{
    $('.report').click(function()
    {
        var url = $(this).attr('data-url');
        showReport(url);
    });

    $('#btnSendClient').on('click', function (e)
    {
        showClientShopsReport($('#frmClient')[0], e);
    });

    $('#btnSendDates').on('click', function (e)
    {
        showDatesReport($('#frmDates')[0], e);
    });
    
    $(document).scroll(function() {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });
    
    $('.scroll-to-top').click(function(event) {
        var $anchor = $(this);
        $('html, body').animate({scrollTop:0}, 'slow');
        event.preventDefault();
    });
    
})(jQuery);

// Funcion para mostrar reportes
function showReport(url)
{
    swal({
        title: 'Seguro que desea mostrar el reporte?',
        text: "Será redirigido al reporte seleccionado.",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#138496',
        cancelButtonColor: '#5A6268',
        confirmButtonText: 'Ver el reporte',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value)
        {
            window.open(url)
        }
    });     
}

// Funcion para mostrar reportes
function showClientShopsReport(form, e)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        showReport('reporteFacturas.jsp?id=' + $('#cId').val());
    }
    form.classList.add('was-validated');
}

// Funcion para mostrar reportes
function showDatesReport(form, e)
{
    if (form.checkValidity() === false)
    {
        e.preventDefault();
        e.stopPropagation();
    }
    else
    {
        var url = 'reporteFacturasRangoFechas.jsp?f1=' + $('#f1').val() 
                + '&f2=' + $('#f2').val();
        showReport(url);
    }
    form.classList.add('was-validated');
}
