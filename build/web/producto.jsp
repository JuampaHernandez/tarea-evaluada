<%-- 
    Document   : producto
    Created on : 09-24-2018, 01:32:12 PM
    Author     : Juan Pablo Elias Hernandez
--%>

<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.models.*;" %>
<%@page session="true" %>

<% 
    HttpSession ss = request.getSession();
    Integer idTu = -1;
    String user = "";
    if (ss.getAttribute("tu") != null || ss.getAttribute("user") != null)
    {
        idTu = (Integer) ss.getAttribute("tu");
        user = (String) ss.getAttribute("user");
    }
    else
    {
        response.sendRedirect("login.jsp");
    }
    DaoProducto daoP = new DaoProducto();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="resources/img/Icon-shop.png" sizes="64x64">
    <link href="resources\css\bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="resources/css/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="resources/css/datatables.css" rel="stylesheet" type="text/css"/>
    <link href="resources/css/dataTables.bootstrap4.css"/>
    <link href="resources/css/buttons.bootstrap4.css" rel="stylesheet" type="text/css"/>
    <title>Producto</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">
            <i class="fas fa-store"></i>
            Tienda
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">
                        <i class="fas fa-home"></i>
                        Inicio <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="producto.jsp">
                        <i class="fas fa-shopping-basket"></i>
                        Productos
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="vendedor.jsp">
                        <i class="fas fa-id-card-alt"></i>
                        Vendedores
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cliente.jsp">
                        <i class="fas fa-users"></i>
                        Clientes
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="venta.jsp">
                        <i class="fas fa-receipt"></i>
                        Venta
                    </a>
                </li>
                <%
                    if (idTu == 1)
                    {
                %>
                <li class="nav-item">
                    <a class="nav-link" href="usuario.jsp">
                        <i class="fas fa-user"></i>
                        Usuarios
                    </a>
                </li>
                <%
                    }
                %>
            </ul>
            <form action="controllerUsuario" id="closeFrm" method="post" class="form-inline my-2 my-lg-0">
                <input type="text" name="close" value="close" style="display: none;">
                <button class="btn btn-outline-light btn-sm my-2 my-sm-0" 
                        name="close" value="close" type="button" id="closeSession">
                    <i class="fas fa-sign-out-alt"></i>
                    Cerrar sesion
                </button>
            </form>
        </div>
    </nav>
    <div class="container">
        <br>
        <br>
        <center>
            <h1><i class="fas fa-shopping-basket"></i> Producto</h1>
        </center>
        <br>
        <%
            if (idTu == 1)
            {
        %>
        <form action="controllerProducto" id="frm" name="frmProductos" method="POST" class="needs-validation" novalidate>
            <div class="form-row">
                <div class="form-group col-md-4" style="display: none;">
                    <input type="number" class="form-control" id="txtId" name="txtId" readonly value="0" required/>
                    <input type="text" name="action" id="action"/>
                </div>
                <div class="form-group col-md-4">
                    <label>Nombre:</label>
                    <input type="text" placeholder="Nombre" class="form-control" id="txtNombre" name="txtNombre" required/>
                    <div class="invalid-feedback">
                        Por favor, ingrese el nombre del producto.
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label>Precio:</label>
                    <input type="number" value="0.0" min="0.75" step="any" class="form-control" id="txtPrecio" name="txtPrecio" required>
                    <div class="invalid-feedback">
                        Por favor, ingrese el precio del producto.
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label>Stock Minimo:</label>
                    <input type="number" value="0" min="1" class="form-control" id="txtStockMinimo" name="txtStockMinimo" required>
                    <div class="invalid-feedback">
                        Por favor, ingrese el stock minimo.
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Stock Actual:</label>
                    <input type="number" value="0" min="0" class="form-control" id="txtStockActual" name="txtStockActual" required>
                    <div class="invalid-feedback">
                        Por favor, ingrese el stock actual.
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label>Codigo de Barra:</label>
                    <input type="text" placeholder="Codigo de barra" class="form-control" id="txtCodigoBarra" name="txtCodigoBarra" required/>
                    <div class="invalid-feedback">
                        Por favor, genere o ingrese un codigo de barras.
                    </div>
                </div>
            </div>
            <center>
                <button type="submit" name="btnAdd" value="Add" class="btn btn-outline-success">
                    <i class="fas fa-plus"></i>
                    Nuevo
                </button>
                <button type="button" name="btnEdit" value="editar" class="btn btn-outline-info btnAction">
                    <i class="fas fa-pencil-alt"></i>
                    Editar
                </button>
                <button type="button" name="btnDelete" value="eliminar" class="btn btn-outline-danger btnAction">
                    <i class="fas fa-trash"></i>
                    Eliminar
                </button>
                <button type="button" name='btnCode' class="btn btn-outline-info btnCode" value="x">
                    <i class="fas fa-barcode"></i>
                    Generar codigo factura
                </button>
                <button type="reset" class="btn btn-outline-secondary">
                    <i class="fas fa-undo"></i>
                    Cancelar
                </button>
            </center>
        </form>
        <br>
        <%
            }
        %>
        <!-- Example DataTables Card -->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Listado de productos
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="dataTable" class="table table-hover text-center">
                        <thead class="">
                            <th scope="col">ID</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Stock Minimo</th>
                            <th scope="col">Stock Actual</th>
                            <th scope="col">Codigo de Barra</th>
                            <th scope="col">Seleccionar</th>
                        </thead>
                        <tbody>
                        <%
                            List<Producto> lst = daoP.mostrar();
                            for(Producto produ : lst)
                            {
                        %>
                        <tr>
                            <td><%= produ.getId()%></td>
                            <td><%= produ.getNombre()%></td>
                            <td><%= produ.getPrecioVenta()%></td>
                            <td><%= produ.getStockMin()%></td>
                            <td><%= produ.getStockAct()%></td>
                            <td><%= produ.getCodigoBarra()%></td>
                            <td><a class="btn btn-outline-primary btn-sm col-12 select" href="javascript:cargar(<%= produ.getId()%>,'<%=produ.getNombre()%>',<%=produ.getPrecioVenta()%>,
                                   <%=produ.getStockMin()%>,<%=produ.getStockAct()%>,'<%= produ.getCodigoBarra()%>')">Seleccionar</a></td>
                        </tr>
                        <%
                            }
                        %>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted">
                Última actualización: 
                <%
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                    String formatDateTime = LocalDateTime.now().format(formatter);
                    out.print(formatDateTime);
                %>
            </div>
        </div>
    </div>
    <script src="resources/js/jquery-3.3.1.js" type="text/javascript"></script>
    <script src="resources\js\popper.min.js" type="text/javascript"></script>
    <script src="resources\js\bootstrap.min.js" type="text/javascript"></script>
    <script src="resources\js\fontawesome-all.js" type="text/javascript"></script>
    <script src="resources\js\sweetalert2.all.min.js" type="text/javascript"></script>
    <script src="resources/js/datatables.js" type="text/javascript"></script>
    <script src="resources/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="resources/js/buttons.bootstrap4.js" type="text/javascript"></script>
    <script src="resources/js/producto.js" type="text/javascript"></script>
    <script src="resources/js/globalConfig.js" type="text/javascript"></script>
    <script>
    <%
        if (request.getSession().getAttribute("info") != null)
        {
            String info = (String) request.getSession().getAttribute("info");
    %>
        swal('Realizado!', '<%= info %>', 'success');
        console.log('Mensaje');
    <%        
    }
    request.getSession().setAttribute("info", null);
    // Error handler
    if (request.getSession().getAttribute("Error") != null)
    {
        String error = (String) request.getSession().getAttribute("Error");
    %>
        swal({
            type: 'error',
            title: 'Error al realizar la operación',
            text: "Ocurrió el siguiente error: \n" + "<%= error %>",
        })
    <%        
        }
        request.getSession().setAttribute("Error", null);
    %>
    </script>
</body>
</html>

