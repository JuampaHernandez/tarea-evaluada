<%-- 
    Document   : reporteClientes
    Created on : 3/10/2018, 12:07:51 PM
    Author     : Juan Pablo Elias Hernández
--%>

<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.io.File"%>
<%@page import="com.connection.Conexion"%>
<%@page import="net.sf.jasperreports.engine.*" %>
<%@page import="net.sf.jasperreports.view.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" type="image/png" href="resources/img/Icon-shop.png" sizes="64x64">
        <title>Reporte clientes</title>
    </head>
    <body>
        <div align="center">
            <br>
            <h1>Reporte de clientes</h1>
            <br>
            <%
                Conexion c = new Conexion();
                c.conectar();
                File report = new File(application.getRealPath("reports/clientes.jasper"));
                Map param = new HashMap();
                byte[] bytes = JasperRunManager.runReportToPdf(
                        report.getPath(), param, c.getConn());
                response.setContentType("application/pdf");
                response.setContentLength(bytes.length);
                ServletOutputStream output = response.getOutputStream();
                response.getOutputStream();
                output.write(bytes, 0, bytes.length);
                output.flush();
                output.close();
            %>
        </div>
    </body>
</html>
